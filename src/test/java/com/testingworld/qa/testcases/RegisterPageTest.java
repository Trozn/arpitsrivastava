package com.testingworld.qa.testcases;



import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.testingworld.qa.base.TestBase;
import com.testingworld.qa.page.RegisterPage;

public class RegisterPageTest extends TestBase{
	static RegisterPage registerPage; 
	
	public RegisterPageTest() {
		super();
	}
	
	@BeforeMethod
	public static void setUp(){
		initialization();
		registerPage = new RegisterPage();
	}
	
  @Test(groups = {"smoke"})
  public void userRegistration()throws Exception{
	  registerPage.registerNewUser();
  }
  
  @Test(groups = {"smoke"})
  public void validateUI(){
	  Assert.assertFalse(registerPage.username.isDisplayed());
  }
  
  @Test(groups ={"nonsmoke"}, enabled = true)
  public void login(){
	  Assert.assertEquals(true, false);
  }
  
  @AfterMethod
  public static void closeBrowser(){
	  tearDown();
  }
}
