package com.testingworld.qa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testingworld.qa.base.TestBase;

public class RegisterPage extends TestBase{
	
	@FindBy(name = "fld_username")
	public 	WebElement username;
	
	@FindBy(name = "fld_email")
	WebElement email;
	
	@FindBy(name = "fld_password")
	WebElement password;
	
	@FindBy(name = "fld_cpassword")
	WebElement confirmPassword;
	
	@FindBy(name = "dob")
	WebElement dateOfBirth;
	
	public RegisterPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void registerNewUser() {
		username.sendKeys("Arpit_0123");
		email.sendKeys("abc1256@gmail.com");
		password.sendKeys("pass@123");
	}
	

}
